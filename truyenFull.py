import bs4
import requests
from requests_html import HTMLSession
session = HTMLSession()
import io

def get_page_content(url):
    page = requests.get(url, headers={"Accept-Language": "en-US"})
    return bs4.BeautifulSoup(page.text, "html.parser")


# các bạn thay link của trang mình cần lấy dữ liệu tại đây
website = 'https://truyenfull.vn/nhat-ngon-thong-thien/'
result = ''
url = 'https://truyenfull.vn/nhat-ngon-thong-thien/quyen-1-chuong-1/'
count = 0
try:
    while url != 'javascript:void(0)':
        print(count)
        count += 1
        session = HTMLSession()
        response = session.get(url)
        soup = get_page_content(url)
        title = response.html.find('title', first=True).text
        content = response.html.find('.chapter-c', first=True).text
        result = result + title + '\n'
        result = result + content + '\n \n'
        next_chap = soup.find_all("a", {"id": "next_chap"})[0]
        url = next_chap['href']
except requests.exceptions.RequestException as e:
    print(e)
with io.open('output.html', "w+", encoding="utf-8") as f:
     f.write(str(result))
f.close()