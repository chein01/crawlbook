import bs4
import requests


def get_page_content(url):
    page = requests.get(url, headers={"Accept-Language": "en-US"})
    return bs4.BeautifulSoup(page.text, "html.parser")


# các bạn thay link của trang mình cần lấy dữ liệu tại đây
url = 'https://metruyenchu.com/truyen/do-de-cua-ta-deu-la-trum-phan-dien' + '/chuong-'

result = ''
count = 0
for i in range(1, 9999):
    print(i)
    count += 1
    # các bạn thay link của trang mình cần lấy dữ liệu tại đây
    url_temp = url + str(i)
    soup = get_page_content(url_temp)
    for e in soup.find_all('br'):
        e.replace_with('')
    title = ''.join(soup.find('title').contents)
    print(url)
    if title == '404':
        break
    else:
        chapter = ''.join(soup.find('div', class_='h1 mb-4 font-weight-normal nh-read__title').contents)
        content = ''.join(map(str, soup.find('div', class_='nh-read__content post-body').contents))
        result += str(chapter) + str(content)
    print('Crawled ' + str(count) + ' Chapter')
file = open("output.html", "w")
file.write(result)
file.close()
